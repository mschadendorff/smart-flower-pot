const int WATER_DURATION = 7000; // nicht länger als 20 sek, Transistor wird sehr heiß
const long WAIT = 720000; // Wartezeit nachdem gegossen wurde: 12h
const int MOISTURE = 500; // Feuchtigkeitswert. Wenn überschritten, wird gegossen
int controlPin = 3;
int emptyTankLEDPin = 8;
int rightSensorPin = A0;
int leftSensorPin = A1;
int waterTankPin = A2;
unsigned long currentMillis;
long previousMillis = -1 * WAIT; // Initialwert auf negative Wartezeit setzen, 
                                 // damit direkt anfang gemessen bzw. gegossen wird
boolean tankEmpty = false;

void setup() {
  pinMode(emptyTankLEDPin, OUTPUT);
  pinMode(controlPin, OUTPUT);
  Serial.begin(1200);  
}

void loop() {
  // Ausgabe der Sensorwerte
  int sensorValueLeft = analogRead(leftSensorPin);
  int sensorValueRight = analogRead(rightSensorPin);
  int sensorValueTank = analogRead(waterTankPin);
  
  String left = "Left: ";
  String right = "Right: ";
  String tank = "Tank: ";
  left += sensorValueLeft;
  right += sensorValueRight;
  tank += sensorValueTank;
  Serial.println(left + "     " + tank + "    " + right);

  // Hauptlogik für Messen und Gießen 
  currentMillis = millis();
 
  tankEmpty = sensorValueTank > 1000 ? true : false;
  if (tankEmpty) {
    warnEmptyTank();
  }
  else {
    notWarnEmptyTank();
    if ((sensorValueLeft + sensorValueRight) / 2 > MOISTURE) {
      startWatering();
      delay(WATER_DURATION);
      stopWatering();
      previousMillis = currentMillis;
      waitAfterWatering();
    } 
  }
  
}

void startWatering() {
  analogWrite(controlPin, 170); // PWM
}

void stopWatering() {
  analogWrite(controlPin, 0);
}

void warnEmptyTank() {
  digitalWrite(emptyTankLEDPin, HIGH);
}

void notWarnEmptyTank() {
  digitalWrite(emptyTankLEDPin, LOW);
}

// jeweils Schleifendurchlauf mit 10 sek Wartezeit,
// bei längeren Delays funktioniert die Funktion nicht richtig
void waitAfterWatering() {
  for (int waitingTime = 0; waitingTime <= WAIT; waitingTime += 10000) {
    delay (10000);
  }
}

